#! python3  # noqa: E265

################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import csv
import glob
import logging
import os
import sqlite3
import string
import subprocess
from pathlib import Path
from shutil import copyfile

# 3rd party embedded into this project
from .external import xlsxwriter
from . import processing_logging

# ############################################################################
# ########## Globals ###############
# ##################################

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)


# ############################################################################
# ########## Functions #############
# ##################################


def log(message, logLevel="info"):
    processing_logging.log("splio: " + str(message), module_logger, logLevel)


def spatialite_connect(*args, **kwargs):
    """returns a dbapi2.Connection to a SpatiaLite db
    using the "mod_spatialite" extension (python3)"""
    import sqlite3

    con = sqlite3.dbapi2.connect(*args, **kwargs)
    con.enable_load_extension(True)
    cur = con.cursor()
    libs = [
        # SpatiaLite >= 4.2 and Sqlite < 3.7.17 (Travis)
        ("mod_spatialite.so", "sqlite3_modspatialite_init"),
        # SpatiaLite >= 4.2 and Sqlite >= 3.7.17, should work on all platforms
        ("mod_spatialite", "sqlite3_modspatialite_init"),
        # SpatiaLite < 4.2 (linux)
        ("libspatialite.so", "sqlite3_extension_init"),
    ]
    found = False
    for lib, entry_point in libs:
        try:
            cur.execute("select load_extension('{}', '{}')".format(lib, entry_point))
        except sqlite3.OperationalError as err:
            if __debug__:
                log(err, "debug")
            continue
        else:
            found = True
            break
    if not found:
        raise RuntimeError("Cannot find any suitable spatialite module")
    cur.close()
    con.enable_load_extension(False)
    return con


def create_spatialite_db(file_name):
    log("Création de la base de donnée Spatialite %s" % (file_name))
    if not Path(file_name).exists():
        con = spatialite_connect(file_name)
        cur = con.cursor()
        # on crée l'extension spatialite
        cur.execute("BEGIN")
        cur.execute("SELECT InitSpatialMetaData();")
        cur.execute("COMMIT")
        cur.close()
        con.close()
    else:
        raise ValueError("La base de donnée %s existe déjà" % (file_name))


def table_exists(db_file, tbl_name):
    """
    Return true if table "tbl_name" exists in base "db_file"
    """
    con = spatialite_connect(db_file)
    cur = con.cursor()
    # On vérifie en SQL si la table existe dans la base
    req = (
        "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name LIKE '%s';"
        % (tbl_name)
    )
    cur.execute(req)
    res = cur.fetchone()[0]
    cur.close()
    con.close()
    if res == 0:
        return False
    else:
        return True


def column_exists(db_file, tbl_name, column_name):
    """
    Return true if column "column_name" exists in table "tbl_name"
    """
    if table_exists(db_file, tbl_name):
        con = spatialite_connect(db_file)
        cur = con.cursor()
        req_search_for_column = "PRAGMA table_info('%s');" % (tbl_name)
        cur.execute(req_search_for_column)
        results = cur.fetchall()
        cur.close()
        con.close()
        columns = [
            row[1] for row in results
        ]  # second column of PRAGMA INFO is column name
        if column_name in columns:
            return True
        else:
            return False


def add_column(db_file, tbl_name, column_name):
    if table_exists(db_file, tbl_name) and not column_exists(
        db_file, tbl_name, column_name
    ):
        log("Ajout de la colonne %s à la table %s" % (column_name, tbl_name), "debug")
        con = spatialite_connect(db_file)
        cur = con.cursor()
        req = "ALTER TABLE '%s' ADD %s " % (tbl_name, column_name)
        cur.executescript(req)
        cur.close()
        con.close()


def get_srs(db_file, tbl_name):
    """
    Return CRS of the table
    """
    con = spatialite_connect(db_file)
    cur = con.cursor()
    log("Vérification du SRS de la table %s dans la base" % (tbl_name), "debug")
    req = "SELECT srid FROM geometry_columns WHERE f_table_name = '%s';" % (tbl_name)
    # log('IMP_SHP : Requete SQL : ' + req)
    cur.execute(req)
    out_sql = cur.fetchone()
    cur.close()
    con.close()

    if out_sql == None:
        log("La table n'a pas de géometrie dans spatialite.", "debug")
        return None
    else:
        srid = out_sql[0]
        return "EPSG:" + str(srid)


def geom_type(db_file, tbl_name):
    """
    Return geom type in text. Returns None in no geometry
    Spatialites geoms :
    1 = POINT
    2 = LINESTRING
    3 = POLYGON
    4 = MULTIPOINT
    5 = MULTILINESTRING
    6 = MULTIPOLYGON
    7 = GEOMETRYCOLLECTION
    """
    con = spatialite_connect(db_file)
    cur = con.cursor()
    log(
        "Vérification de la géometrie de la table %s dans la base" % (tbl_name), "debug"
    )
    req = "SELECT geometry_type FROM geometry_columns WHERE f_table_name = '%s';" % (
        tbl_name
    )
    # log('IMP_SHP : Requete SQL : ' + req)
    cur.execute(req)
    out_sql = cur.fetchone()
    cur.close()
    con.close()

    if out_sql is None:
        log("La table n'a pas de géometrie dans spatialite.", "debug")
        return None
    else:
        dict_geoms = {
            1: "POINT",
            2: "LINESTRING",
            3: "POLYGON",
            4: "MULTIPOINT",
            5: "MULTILINESTRING",
            6: "MULTIPOLYGON",
            7: "GEOMETRYCOLLECTION",
        }
        geom_num = out_sql[0]
        try:
            return dict_geoms[geom_num]
        except IndexError:
            raise ValueError("Undefined geometry (%s)" % (geom_num))
    #     if (geom == 4) or (geom == 5) or (geom == 6) or (geom == 7):
    #         log('Geometrie de type multi detectée', 'debug')
    #         return 'multi'
    #     else:
    #         log('Géometrie de type mono detectée', 'debug')
    #         return 'mono'
    # #On ferme la connection spatialite


def import_file_to_spl(ogr_target, shp_csv_file, tbl_name, options="", prefix=None):
    """
    Import un fichier shp ou csv vers une base spatialite avec ogr2ogr.
    Le nom de la table sera le nom du fichier en minuscule sans l'extension

        option : ajouter des options à la commande ogr2ogr
            (par exemple -skipfailures)
        prefix : si différent de '', ajoutera une colonne "import_prefix" à la
            table. Chaque enregistrement prendra la valeur donnée en paramètre.
    """
    log(
        "### Import du fichier %s dans la table %s de la bdd %s"
        % (shp_csv_file, tbl_name, ogr_target)
    )

    file_type = os.path.splitext(os.path.basename(shp_csv_file))[1]
    if file_type not in (".csv", ".shp"):
        raise ValueError("Le type de fichier %s n'est pas supporté" % (file_type))

    # Construction de la commande ogr2ogr
    # On commence par les options passées en paramètre
    ogr_options = options

    if file_type == ".shp":
        # ajout d'une option OGR
        ogr_options += " -dim XY"
        if not table_exists(ogr_target, tbl_name):
            log("La table " + tbl_name + " n'existe pas. Elle sera créée par org2ogr")
            # On ajoute l'option pour une geometrie multi et on donne le nom "geom" pour la
            # colonne de géometrie (sinon nom arbitraire OGR moche)
            # ajout d'une option OGR
            ogr_options += " -nlt PROMOTE_TO_MULTI  -lco GEOMETRY_NAME=geom"
        # Sinon la table existe, on vérifie sa geometry pour les parametrès d'import OGR
        else:
            log("La table %s existe déjà. Les données seront ajoutées" % (tbl_name))
            geom_type_ = geom_type(ogr_target, tbl_name)
            if geom_type_ in ("POINT", "LINESTRING", "POLYGON"):
                pass
            elif geom_type_ in ("MULTIPOINT", "MULTILINESTRING", "MULTIPOLYGON"):
                # ajout d'une option OGR
                log(
                    "La table de destination à une géometrie multi. "
                    "Ajout du paramètre PROMOTE_TO_MULTI",
                    "debug",
                )
                ogr_options += " -nlt PROMOTE_TO_MULTI"
            ogr_options += ' -t_srs "%s"' % (get_srs(ogr_target, tbl_name))
            # Other option is :
            # ogr_options += ' -nlt %s' %(geom_type_)
            # but this option will create invalid geoms without warning
    elif file_type == ".csv":
        # ajout d'une option OGR
        ogr_options += " -oo EMPTY_STRING_AS_NULL=YES"

    if prefix is not None and prefix != "":
        if table_exists(ogr_target, tbl_name) and not column_exists(
            ogr_target, tbl_name, "import_prefix"
        ):
            add_column(ogr_target, tbl_name, "import_prefix")

        # S'il y a un préfixe on ajoute une option -sql pour ajouter une colonne
        shp_name_without_extension = os.path.splitext(os.path.basename(shp_csv_file))[0]
        # ajout d'une option OGR
        ogr_options += " -sql  \"SELECT *, '%s' AS import_prefix FROM %s\"" % (
            prefix,
            shp_name_without_extension,
        )
        # Avec l'ajout de cette option, la requête plante si le nom du shp contient des charactères spéciaux
        # On gère alors le cas proprement avec une exception
        # Liste des charactères interdits (underscore _ autorisé)
        invalidChars = set(string.punctuation.replace("_", ""))
        if any(char in invalidChars for char in shp_name_without_extension):
            raise ValueError(
                "Impossible d'ajouter un préfixe d'import si les "
                "noms de shp contiennent des caractères spéciaux. "
                "Essayer en renommant les shapes à importer."
            )

    # On assemble la commande d'appel à OGR2OGR
    cmd = 'ogr2ogr -f sqlite "%s" "%s" -nln %s -append %s' % (
        ogr_target,
        shp_csv_file,
        tbl_name,
        ogr_options,
    )
    log("OGR2OGR : %s" % (cmd))
    proc = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        encoding="utf-8",
    )
    # récupération des sorties éventuelles
    (out, err) = proc.communicate()
    # On renvoi un dictionnaire comprenant la commande d'appel et les sorties de OGR
    if err != "":  # la commande renvoi une erreur ou un warning
        log(err, "warning")
        if "ERROR" in err:
            if "-skipfailure" not in options:  # On évite une boucle infinie
                log(
                    "### Erreur lors de l'import. Nouvel essai avec l'option -skipfailure"
                )
                # Si la commande renvoir une erreur on réessaie avec l'option -skipfailures (import ligne à ligne)
                import_file_to_spl(
                    ogr_target,
                    shp_csv_file,
                    tbl_name,
                    options + " -skipfailures",
                    prefix,
                )
            else:
                log(
                    "### Erreur lors de l'import. Toutes les données n'ont "
                    "peut-être pas été importées\n\n",
                    "warning",
                )
        else:
            log("### Fin de l'import\n\n")
    else:
        log("### Fin de l'import\n\n")


def import_folder_to_spl(
    folder,
    ogr_target=None,
    prefix=None,
    include_csv=False,
    ignore_grace_list=True,
    recursive=False,
):
    if recursive:
        shp_glob_pattern = "**/*.shp"
        csv_glob_pattern = "**/*.csv"
    else:
        shp_glob_pattern = "*.shp"
        csv_glob_pattern = "*.csv"

    if ogr_target is None:
        log("Aucune base de donnée renseignée. Création d'une nouvelle BDD")
        ogr_target_ = str(Path(folder) / "base.sqlite")
        create_spatialite_db(ogr_target_)
    elif Path(ogr_target).exists():
        log("Création d'une sauvegarde de la BDD")
        copyfile(ogr_target, ogr_target + "_backup")
        ogr_target_ = ogr_target
    else:
        raise ValueError("La base de donnée %s n'existe pas" % (ogr_target))

    if include_csv:
        list_files = list(Path(folder).glob(shp_glob_pattern)) + list(
            Path(folder).glob(csv_glob_pattern)
        )
        log("Option CSV activée. Liste des fichier à importer :")
        for i in list_files:
            log(str(i))
    else:
        list_files = list(Path(folder).glob(shp_glob_pattern))
        log("Liste des fichier à importer :")
        for i in list_files:
            log(str(i))

    for file in list_files:
        if ignore_grace_list and file.name.startswith("l_"):
            log(
                "le fichier %s sera ignoré car il correspond une liste de valeur GRACE THD"
                % (os.path.basename(file))
            )
        else:
            # On prend le nom de fichier, sans l'extension et en minuscule
            # sera le nom de la table sqlite
            tbl_name = file.stem.lower()
            # On remplace les espaces par _ dans le nom
            tbl_name = tbl_name.replace(" ", "_")
            if recursive:
                prefix = Path(file).parent.stem
            import_file_to_spl(ogr_target_, file, tbl_name, prefix=prefix)


def extract_table_to_file(
    spl_db, view_name, dir_name=None, file_name=None, sheet_name=None, file_type="csv"
):
    """Extraction de la table ou vue dans un fichier csv ou xls ou shp"""

    # si les paramètres dir_name et file_name ne sont pas passés on les initialise
    if dir_name is None:
        dir_name = os.path.dirname(spl_db)
    if file_name is None:
        file_name = view_name
    if sheet_name is None:
        sheet_name = view_name

    targetfilepath = str(Path(dir_name) / (file_name + "." + file_type))
    # si csv ou xlsx, on fait une requête via l'api python spatialite
    if file_type in ["csv", "xlsx"]:

        log("Calcul de la vue %s" % (view_name))
        spl_connection = spatialite_connect(spl_db)
        with spl_connection:
            # On modifie row_factory pour qu'il utilise sqlite3.Row (permet d'utiliser les retours comme un dict ou comme une liste
            spl_connection.row_factory = sqlite3.Row
            spl_cursor = spl_connection.cursor()
            req = "SELECT * FROM %s;" % (view_name)
            spl_cursor.execute(req)
        # récupération des résultats de la requête
        rows = spl_cursor.fetchall()
        headers = [i[0] for i in spl_cursor.description]
        # tester : headers = rows.keys()
        spl_connection.close()

        if file_type == "csv":
            log("Export dans le fichier  %s au format %s" % (file_name, file_type))
            # écriture dans un csv
            fp = open(targetfilepath, "w")
            csv_file = csv.writer(fp, lineterminator="\n", delimiter=";")
            csv_file.writerow(headers)
            csv_file.writerows(rows)
            fp.close()

        elif file_type == "xlsx":
            log("Export dans le fichier  %s au format %s" % (file_name, file_type))
            workbook = xlsxwriter.Workbook(targetfilepath)
            worksheet = workbook.add_worksheet()
            worksheet.write_row(0, 0, headers)
            # -- Modif pour éviter plantage si colonne geom dans l'export
            #            for i,row in enumerate(rows,1):
            #                worksheet.write_row(i, 0, row)
            for i, row in enumerate(rows, 1):
                for j, value in enumerate(row):
                    # Risque de plantage si la valeur n'est pas de type int, float ou str. Donc on n'écrit pas la valeur si type bytes
                    if type(value) != bytes:
                        worksheet.write(i, j, value)
            # -- Fin modif -----------------------------------------------
            workbook.close()

    # si shp, on fait une requête via ogr
    elif file_type == "shp":
        cmd = 'ogr2ogr -f "ESRI Shapefile" "%s" "%s" -sql "SELECT * FROM %s"' % (
            targetfilepath,
            spl_db,
            view_name,
        )
        log("Commande OGR : \n" + cmd, "debug")
        proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
            encoding="utf-8",
        )
        # récupération des sorties éventuelles
        (out, err) = proc.communicate()
        log(out, "debug")
        log(err, "debug")
    elif file_type == "sqlite":
        cmd = (
            'ogr2ogr -f sqlite "%s" "%s" -sql "SELECT * FROM %s" -nln %s -append -overwrite -dsco SPATIALITE=YES'
            % (targetfilepath, spl_db, view_name, view_name)
        )
        log("Commande OGR : \n" + cmd, "debug")
        proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
            encoding="utf-8",
        )
        # récupération des sorties éventuelles
        (out, err) = proc.communicate()
        log(out, "debug")
        log(err, "debug")

    return targetfilepath


def extract_tables_to_xlsx_sheets(
    spl_db, list_tables, file_name, dir_name=None, list_sheet_name=None
):
    """Extraction de la table ou vue dans un fichier xlsx avec un onglet par table"""
    if dir_name is None:
        dir_name = os.path.dirname(spl_db)
    if list_sheet_name is None:
        list_sheet_name = list_tables

    # Création et ouverture du fichier excel
    targetfilepath = str(Path(dir_name) / (file_name + ".xlsx"))
    log("Création du fichier xlsx %s" % (targetfilepath))
    workbook = xlsxwriter.Workbook(targetfilepath)

    for num, table in enumerate(list_tables):
        # Requête de la BDD
        log("Calcul de la vue %s" % (table))
        spl_connection = spatialite_connect(spl_db)
        with spl_connection:
            # On modifie row_factory pour qu'il utilise sqlite3.Row (permet d'utiliser les retours comme un dict ou comme une liste
            spl_connection.row_factory = sqlite3.Row
            spl_cursor = spl_connection.cursor()
            req = "SELECT * FROM %s;" % (table)
            spl_cursor.execute(req)
        # récupération des résultats de la requête
        rows = spl_cursor.fetchall()
        headers = [i[0] for i in spl_cursor.description]
        # tester : headers = rows.keys()
        spl_connection.close()

        # Création d'un nouvel onglet
        worksheet = workbook.add_worksheet(list_sheet_name[num])
        worksheet.write_row(0, 0, headers)
        for i, row in enumerate(rows, 1):
            for j, value in enumerate(row):
                # Risque de plantage si la valeur n'est pas de type int, float ou str. Donc on n'écrit pas la valeur si type bytes
                if type(value) != bytes:
                    worksheet.write(i, j, value)
    # Ecriture et fermeture du xlsx
    workbook.close()
    return targetfilepath


def execute_query_to_spl(sql_query, spl_db):
    con = spatialite_connect(spl_db)
    # on execute le fichier sql en tant que script
    log("Exécution du script sur la base de donnée")
    with con:
        cur = con.cursor()
        cur.executescript(sql_query)
    con.close()


def execute_file_to_spl(filename, spl_db):
    """Exécute le script .sql cible dans la base de donnée spl_db.
    si le fichier est un dossier, execute chaque fichier .sql dans l'ordre alphabétique"""
    # si le fichier est un fichier .sql
    if Path(filename).suffix == ".sql":
        # on lit le fichier sql
        log("Lecture du fichier %s" % (filename))
        with open(filename, "r") as f:
            sql_script = f.read()
        log("Connexion à la base de donnée %s" % (spl_db))
        con = spatialite_connect(spl_db)
        # on execute le fichier sql en tant que script
        log("Exécution du script sur la base de donnée")
        with con:
            cur = con.cursor()
            cur.executescript(sql_script)
        con.close()
    # si le fichier est un dossier, on liste les fichier *.sql et on execute la fonction
    elif Path(filename).is_dir():
        list_file_sql = [file for file in Path(filename).glob("*.sql")]
        # tri par ordre alphabétique
        list_file_sql.sort()
        log(
            "Le fichier cible est un dossier. Liste des fichiers *.sql trouvés dans ce dossier : "
        )
        for f in list_file_sql:
            log(f)
        for f in list_file_sql:
            execute_file_to_spl(f, spl_db)
    # sinon on renvoit une erreur
    else:
        raise ValueError("Le fichier cible n'est ni un fichier *.sql ni un dossier")


def export_preset_tables(spatialite_db, config_table_name):
    """
    On récupère le contenu de la table config_table_name
    Cette table doit avoir le format suivant :
       view_name VARCHAR(254),    --> nom de la table ou vue à exporter
       dest_file VARCHAR(254),    --> nom du fichier de destination sans extention
       dest_sheet VARCHAR(254),   --> nom de l'onglet du fichier (si plusieurs fois le même fichier en xlsx)
       dest_folder VARCHAR(254),  --> chemin du sous-dossier d'export (chemin relatif). Doit commencer par "/"
       file_type VARCHAR(254)     --> type (csv, xlsx, shp)
    """

    log("Récupération de la liste des vues de contrôle à exporter")
    con = spatialite_connect(spatialite_db)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    req = "SELECT * FROM %s;" % (config_table_name)
    cur.execute(req)
    rows = cur.fetchall()
    # Fermeture de la connexion sql
    cur.close()
    con.close()

    files = dict()
    for row in rows:
        # On traite ligne par ligne.
        # Récupération des paramètres
        file_name = row["dest_file"]
        view_name = row["view_name"]
        sheet_name = row["dest_sheet"]
        file_type = row["file_type"]
        # On construit le chemin absolu dans dest_folder
        dest_folder = str(
            Path(spatialite_db)
            .parents[0]
            .joinpath(row["dest_folder"].strip("/").strip("\\"))
        )

        # Si le type de fichier est xlsx et qu'il y a un nom d'onglet (sheet_name) il faut gérer les onglets
        # On stock les données dans un dictionnaire temporaire "files"
        # Chaque entrée de "files" compredra une liste de vues/tables et une liste de nom d'onglets associées.
        # On traitera ensuite la création de xlsx plus bas, en parcourant le dict.
        if file_type == "xlsx" and sheet_name != "":
            if file_name in files.keys():
                files[file_name]["views"].append(view_name)
                files[file_name]["sheets"].append(sheet_name)
                files[file_name]["folder"] = dest_folder
            else:
                files[file_name] = dict()
                files[file_name]["views"] = [view_name]
                files[file_name]["sheets"] = [sheet_name]
                files[file_name]["folder"] = dest_folder
        # Si pas besoin de gérer les onglets, on fait un export simple.
        else:
            log(
                "Extraction de la vue %s au format %s dans le dossier %s (fichier %s)"
                % (view_name, file_type, dest_folder, file_name)
            )
            log("Dossier de destination : %s" % (dest_folder))
            if not os.path.exists(dest_folder):
                os.makedirs(dest_folder)
            extract_table_to_file(
                spatialite_db,
                view_name,
                dir_name=dest_folder,
                file_name=file_name,
                sheet_name=sheet_name,
                file_type=file_type,
            )

    # On parcour le dict pour créer des fichiers xlsx avec onglets.
    for file in files:
        log(
            "Export du fichier %s avec un onglet pour chacune des tables %s"
            % (file, files[file]["views"])
        )
        log("Dossier de destination : %s" % (dest_folder))
        if not os.path.exists(files[file]["folder"]):
            os.makedirs(files[file]["folder"])
        extract_tables_to_xlsx_sheets(
            spatialite_db,
            files[file]["views"],
            file,
            dir_name=files[file]["folder"],
            list_sheet_name=files[file]["sheets"],
        )
    log("Fin des exports")


def check_preset_tables(spatialite_db, config_table_name):
    """
    Appelle les tables/vues définies dans la table "config_table_name" et
    vérifie que leur appel de génère pas une erreur SQL. Fonction à effectuer
    sur un template vide, sinon il y aura un temps de calcul des vues selon
    la quantité de données.

    Rappel structure table "config_table_name"
           view_name VARCHAR(254),    --> nom de la table ou vue à exporter
           dest_file VARCHAR(254),    --> nom du fichier de destination sans extention
           dest_sheet VARCHAR(254),   --> nom de l'onglet du fichier (si plusieurs fois le même fichier en xlsx)
           dest_folder VARCHAR(254),  --> chemin du sous-dossier d'export (chemin relatif). Doit commencer par "/"
           file_type VARCHAR(254)     --> type (csv, xlsx, shp)
    """
    log(
        "Vérification de la validité des tables/vues listées dans la table"
        "%s" % (config_table_name)
    )
    con = spatialite_connect(spatialite_db)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    req = "SELECT * FROM %s;" % (config_table_name)
    cur.execute(req)
    rows = cur.fetchall()
    # Fermeture de la connexion sql
    cur.close()
    con.close()
    for row in rows:
        view_name = row["view_name"]
        try:
            con = spatialite_connect(spatialite_db)
            cur = con.cursor()
            req = "SELECT * FROM '%s'" % (view_name)
            cur.execute(req)
            cur.close()
            con.close()
            log("%s : OK" % (view_name))
        except Exception as e:
            log("%s : NOK" % (view_name), "warning")
            log(e, "warning")


def delete_rows(
    db_file, value, table=None, column="import_prefix", condition="CONTIENT"
):
    con = spatialite_connect(db_file)
    cur = con.cursor()
    req = " PRAGMA foreign_keys = off; "
    cur.executescript(req)

    if table is None:
        tables = list_tables(db_file, column_name=column)
    else:
        tables = [table]

    log(
        "Suppression des lignes répondant à la condition %s %s %s"
        % (column, condition, value)
    )
    log("Dans les tables suivantes : %s" % (tables))
    for table_ in tables:
        if condition == "EGAL":
            req_del = " DELETE FROM '%s' WHERE %s = '%s' " % (table_, column, value)
            req_count = " SELECT COUNT(*) FROM '%s' WHERE %s = '%s';" % (
                table_,
                column,
                value,
            )
        elif condition == "CONTIENT":
            req_del += " DELETE FROM '%s' WHERE %s LIKE '%%%s%%' " % (
                table_,
                column,
                value,
            )
            req_count = " SELECT COUNT(*) FROM '%s' WHERE %s LIKE '%%%s%%';" % (
                table,
                column,
                value,
            )

        cur.execute(req_count)
        n_lines_to_delete = cur.fetchone()[0]
        log("Table %s : %s lignes à supprimer" % (table_, n_lines_to_delete))
        cur.executescript(req_del)

    cur.close()
    con.close()


def list_tables(db_file, column_name=None):

    con = spatialite_connect(db_file)
    cur = con.cursor()
    req = " SELECT name FROM sqlite_master WHERE type='table' "
    cur.execute(req)
    rows = cur.fetchall()
    cur.close()
    con.close()
    if column_name is None:
        tables = [row[0] for row in rows]
    else:
        tables = []
        for row in rows:
            table = row[0]
            if column_exists(db_file, table, column_name):
                tables.append(table)

    return tables
