#! python3  # noqa: E265

################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import logging

# 3rd party included in QGIS
import xlrd

# package
from . import processing_logging


# ############################################################################
# ########## Globals ###############
# ##################################

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)


# ############################################################################
# ########## Functions #############
# ##################################


def log(message: str, logLevel: str = "info"):
    """Map log message on different logging pipes. Mainly: QGIS Processing log system.

    Args:
        message (str): [description]
        logLevel (str, optional): [description]. Defaults to "info".
    """
    processing_logging.log(message, module_logger, logLevel)


def read_sheet_to_dict(
    excel_file_path, list_necessary_columns, primary_key=None, excel_sheet_name=None
):
    """
    Lecture d'un fichier excel vert un dict de dict.
    Chaque ligne est un dict avec comme clés les en-têtes de colones.
    Le fichier est un dict de lignes avec comme clé la valeur \
    dans la colonne "primary_key".
    Si primary_key == None, on utilise le numéro de ligne
    ex :

    le tableau

    Primary_key     |   colonne1    |   colonne2
    key1            |   value1      |   value2
    key2            |   value3      |   value4

    donnera en sortie un dict
    {
        key1 {
            Primary_key : key1,
            colonne1 : value1,
            colonne2 : value2
            },
        key2 {
            Primary_key : key2,
            colonne1 : value3,
            colonne2 : value4
            }
        }
    """
    # Ouverture fichier excel
    wb = xlrd.open_workbook(excel_file_path)

    # Si pas de sheet_name passé en paramètre, on ouvre le premier onglet
    if excel_sheet_name is None:
        excel_sheet = wb.sheet_by_name(wb.sheet_names()[0])
    # Sinon on ouvre l'onglet passé en paramètre
    else:
        excel_sheet = wb.sheet_by_name(excel_sheet_name)
    dict_output = dict()
    headers = dict()
    # parcour un onglet et retour un dict()
    for i, row in enumerate(excel_sheet.get_rows()):
        # Première ligne, on lit les titres de colonne
        if i == 0:
            # on lit chaque cellule de la première ligne
            for j, cel in enumerate(row):
                # On renseigne un dict de telle sorte que: {"titre de la colonne": position}.
                if len(str(cel.value)) > 0:
                    headers[str(cel.value)] = j
            # Si on a pas chaque colonne obligatoire, on renvoit une erreur et on stop la fonction
            # print(headers)
            if not set(list_necessary_columns).issubset(set(headers.keys())):
                raise ValueError(
                    "Les colonnes obligatoires n'ont pas été trouvées dans l'onglet excel"
                )
                return None
        # Pour les autres lignes on lit chaque ligne, on retrouve la "clé" (header) grace au dict header
        else:
            row_dict = dict()
            for j in headers:
                # on lit chaque case grace au dict header
                row_dict[j] = (
                    str(row[headers[j]].value)
                    if len(str(row[headers[j]].value))
                    else None
                )
            # Si pas de paramètre primary_key, on prend le numéro de ligne comme index de dict
            # Sinon on prend la valeur trouvée dans la ligne
            if primary_key is None:
                primary_key_ = i
            else:
                primary_key_ = row_dict[primary_key]
            dict_output[primary_key_] = row_dict.copy()
    return dict_output


def read_all_sheet_to_dict(excel_file_path, list_necessary_columns, primary_key):
    dict_output = dict()
    wb_excel_file = xlrd.open_workbook(excel_file_path)
    for sheet_name in wb_excel_file.sheet_names():
        dict_output.update(
            read_sheet_to_dict(
                excel_file_path,
                list_necessary_columns,
                primary_key,
                excel_sheet_name=sheet_name,
            )
        )
    return dict_output


def read_sheet_to_tab(excel_file_path, excel_sheet_name=None):
    """Lecture d'un fichier excel et retour un dict de deux liste :
    une liste header
    une double liste data"""

    # Ouverture du xlsx
    wb = xlrd.open_workbook(excel_file_path)

    # Si pas de sheet_name passé en paramètre, on ouvre le premier onglet
    if excel_sheet_name is None:
        excel_sheet = wb.sheet_by_name(wb.sheet_names()[0])
    # Sinon on ouvre l'onglet passé en paramètre
    else:
        excel_sheet = wb.sheet_by_name(excel_sheet_name)

    # stockage des valeurs dans un tableau
    header = []
    data = []
    for i, row in enumerate(excel_sheet.get_rows()):
        if i == 0:
            for cel in row:
                header.append(cel.value)
        else:
            data.append([])
            for cel in row:
                data[i - 1].append(cel.value)
    return {"header": header, "data": data}


# ############################################################################
# ########## Main ##################
# ##################################

# for compatibility after simplifying name
read_excel_sheet_to_dict = read_sheet_to_dict

# for compatibility after simplifying name
read_excel_sheet_to_tab = read_sheet_to_tab
