################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import datetime
import sys
from pathlib import Path

# Ajout du dossier racine au path pour permettre leur import. En effet le contenu
# du fichier est exécuté à l'ouverture de QGIS lors du chargement du module
# processing.
root_module_path = Path(__file__).parents[1]  # dossier racine de mcd_check
if root_module_path not in sys.path:
    sys.path.append(str(root_module_path))
################################################################################

# QGIS
from PyQt5.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterFile,
)

# package
from quickinteg import mcd_check, processing_logging


# ############################################################################
# ########## Classes ###############
# ##################################


class Mcd_check_config(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT_CATALOG = "INPUT_CATALOG"
    INPUT_CONFIG = "INPUT_CONFIG"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Mcd_check_config()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "mcd_check_config_to_sql_v2.0"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("mcd_check_config_to_sql_v2.0")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Tactis_Quickinteg")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Tactis_Quickinteg"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("Créer une configuration de contrôle GRACE THD")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        # Input catalogue de contrôle
        desc = "\nCatalogue de contrôle au format xlsx"
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_CATALOG,
                desc,
                behavior=QgsProcessingParameterFile.File,
                optional=False,
                extension="xlsx",
            )
        )

        # Input configuration de contrôle
        desc = "\nConfiguration de contrôle au format xlsx"
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_CONFIG,
                desc,
                behavior=QgsProcessingParameterFile.File,
                optional=False,
                extension="xlsx",
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        processing_logging.log_in_qgis_processing = True
        processing_logging.log_in_qgis_processing_method = feedback.pushInfo

        # Récupération des inputs
        catalogue_file = Path(
            self.parameterAsString(parameters, self.INPUT_CATALOG, context)
        )
        config_controle_file = Path(
            self.parameterAsString(parameters, self.INPUT_CONFIG, context)
        )

        # Création du sous-dossier de sortie
        time_suffix = datetime.datetime.strftime(
            datetime.datetime.now(), "%Y-%m-%d_%H%M%S"
        )
        config_name = config_controle_file.stem

        new_output_sql_folder = config_controle_file.parents[1].joinpath(
            "3_config_ctrl_sql", config_name + "_" + time_suffix
        )
        # Création du dossier
        new_output_sql_folder.mkdir(parents=True)

        sql_code = mcd_check.translate_config_to_sql(
            catalogue_file, config_controle_file
        )
        output_file_path = new_output_sql_folder / "v_ctrl_all.sql"
        with output_file_path.open("w") as f:
            f.write(sql_code)
        # output_file = open(output_file_path,'w')
        # output_file.write(sql_code)
        # output_file.close()
        feedback.pushInfo(
            "Terminé. Résultat dans le fichier\n" "%s" % (output_file_path)
        )
        # Je n'ai pas très bien compris ce qu'il faut retourner ici, mais si on ne retourne rien ça plante.
        return {"output_file": output_file_path}
