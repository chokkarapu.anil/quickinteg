#! python3  # noqa: E265

################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# ############################################################################
# ########## Libraries #############
# ##################################

# standard
from pathlib import Path
import sys

# Ajout du dossier racine au path pour permettre leur import. En effet le contenu
# du fichier est exécuté à l'ouverture de QGIS lors du chargement du module
# processing.
root_module_path = Path(__file__).parents[1]  # dossier racine de mcd_check
if root_module_path not in sys.path:
    sys.path.append(str(root_module_path))
# ##############################################################################

from PyQt5.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterFile,
)

# package
from quickinteg import spatialiteio, processing_logging


# ############################################################################
# ########## Classes ###############
# ##################################


class Template_create(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT_BASE = "INPUT_BASE"
    INPUT_FOLDER_SQL_TABLE_DEFINITION = "INPUT_FOLDER_SQL_TABLE_DEFINITION"
    INPUT_FILE_SQL_CONTROLE_CONFIG = "INPUT_FILE_SQL_CONTROLE_CONFIG"
    INPUT_FILE_SQL_CONTROLE_SYNTHESE = "INPUT_FILE_SQL_CONTROLE_SYNTHESE"
    INPUT_FOLDER_SQL_VARIOUS = "INPUT_FOLDER_SQL_VARIOUS"
    OUTPUT_FOLDER_DESTINATION = "OUTPUT_FOLDER_DESTINATION"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Template_create()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "template_create_v2.0"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("template_create_v2.0")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Tactis_Quickinteg")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Tactis_Quickinteg"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("Créer une base template GRACE THD")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # Input table définition folder
        desc = "\nDossier contenant les scripts SQL de définition des tables."
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_FOLDER_SQL_TABLE_DEFINITION,
                desc,
                behavior=QgsProcessingParameterFile.Folder,
                optional=True,
            )
        )
        # Input configuration de contrôle
        desc = "\nConfiguration de contrôle au format SQL."
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_FILE_SQL_CONTROLE_CONFIG,
                desc,
                behavior=QgsProcessingParameterFile.File,
                optional=True,
                extension="sql",
            )
        )
        # Input synthèse contrôles
        desc = "\nTables de synthèse au format SQL."
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_FILE_SQL_CONTROLE_SYNTHESE,
                desc,
                behavior=QgsProcessingParameterFile.File,
                optional=True,
                extension="sql",
            )
        )
        # Input requêtes diverses
        desc = "\nDossier de requêtes SQL additionnelles diverses."
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_FOLDER_SQL_VARIOUS,
                desc,
                behavior=QgsProcessingParameterFile.Folder,
                optional=True,
                extension="sql",
            )
        )
        # Input output folder
        desc = "\nDossier de destination de la base spatialite (si création d'une nouvelle base)."
        self.addParameter(
            QgsProcessingParameterFile(
                self.OUTPUT_FOLDER_DESTINATION,
                desc,
                behavior=QgsProcessingParameterFile.Folder,
                optional=True,
                extension="sql",
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        processing_logging.log_in_qgis_processing = True
        processing_logging.log_in_qgis_processing_method = feedback.pushInfo

        # Récupération des inputs
        # input_base = self.parameterAsString(parameters,self.INPUT_BASE,context)
        sql_tables_def_folder = self.parameterAsString(
            parameters, self.INPUT_FOLDER_SQL_TABLE_DEFINITION, context
        )
        sql_controle_config_file = self.parameterAsString(
            parameters, self.INPUT_FILE_SQL_CONTROLE_CONFIG, context
        )
        sql_synth_file = self.parameterAsString(
            parameters, self.INPUT_FILE_SQL_CONTROLE_SYNTHESE, context
        )
        sql_various_folder = self.parameterAsString(
            parameters, self.INPUT_FOLDER_SQL_VARIOUS, context
        )
        out_folder = self.parameterAsString(
            parameters, self.OUTPUT_FOLDER_DESTINATION, context
        )

        # on créer une base dans le dossier output
        # Le nom de la base est détermné par le dossier de définition des tables (ex : "gracethd_v2.0.1")
        base_spl_filename = Path(sql_tables_def_folder).name
        base_spl_path = Path(out_folder) / (base_spl_filename + ".sqlite")
        # On crée une base sqlite3 avec extension spatialite
        if base_spl_path.exists():
            raise ValueError("Le fichier %s existe déjà" % (base_spl_path))
        else:
            spatialiteio.create_spatialite_db(base_spl_path)

        # On exécute les différents scripts SQL sur la base spatialite

        # Définitions des Tables
        if sql_tables_def_folder != "":
            spatialiteio.execute_file_to_spl(sql_tables_def_folder, base_spl_path)

        # Configuration de contrôle
        if sql_controle_config_file != "":
            spatialiteio.execute_file_to_spl(sql_controle_config_file, base_spl_path)

        # Table de synthèse
        if sql_synth_file != "":
            spatialiteio.execute_file_to_spl(sql_synth_file, base_spl_path)

        # Requêtes diverses
        if sql_various_folder != "":
            spatialiteio.execute_file_to_spl(sql_various_folder, base_spl_path)

        spatialiteio.check_preset_tables(base_spl_path, "tactis_list_views_to_export")

        return {"output file": base_spl_path}
