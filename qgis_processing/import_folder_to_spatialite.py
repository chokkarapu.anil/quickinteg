#! python3  # noqa: E265

################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import logging
import os
from pathlib import Path
import sys

# Ajout du dossier racine au path pour permettre leur import. En effet le contenu
# du fichier est exécuté à l'ouverture de QGIS lors du chargement du module
# processing.
root_module_path = Path(__file__).parents[1]  # dossier racine de mcd_check
if root_module_path not in sys.path:
    sys.path.append(str(root_module_path))
# ###############################################################################

# QGIS
from PyQt5.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterEnum,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterString,
    QgsProcessingParameterFile,
)

# package
from quickinteg import spatialiteio, processing_logging


# ############################################################################
# ########## Classes ###############
# ##################################


class Import_folder_to_spatialite(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    IN_FOLDER = "IN_FOLDER"
    APPEND = "APPEND"
    #    LAYER_NAME = 'LAYER_NAME'
    BASE_NAME = "BASE_NAME"
    PREFIX = "PREFIX"
    OUT_BDD_NAME = "OUT_BDD_NAME"
    INCLUDE_CSV = "INCLUDE_CSV"
    EXPORT_VIEWS = "EXPORT_VIEWS"
    RECURSIVE = "RECURSIVE"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Import_folder_to_spatialite()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "import_folder_to_spatialite_v.2.0"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("import_folder_to_spatialite_v2.0")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Tactis_Quickinteg")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Tactis_Quickinteg"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Import de fichiers shp/csv vers une base Spatialite(.sqlite)\n"
            "Les fichier doivent être dans un même dossier, sans caractères spéciaux dans les noms de fichier."
            "\n Le nom des tables de destination sera le nom des fichiers, en minuscule."
            "\n en mode réccursif, il faut une arborescence du type : "
            "\nDOSSIER_CIBLE"
            "\n      ├───REF_LIVRABLE_1"
            "\n      │       ├───xx.shp"
            "\n      │       ├───yy.shp"
            "\n      ├───REF_LIVRABLE_2"
            "\n      │       ├───xx.shp"
            "\n      │       ├───yy.shp"
            "\n      ├───REF_LIVRABLE_N..."
            "\n\nPossibilité de créer une nouvelle base ou d'alimenter une base existante"
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        self.addParameter(
            QgsProcessingParameterFile(
                self.IN_FOLDER,
                "Dossier comprenant les fichiers .shp",
                behavior=QgsProcessingParameterFile.Folder,
            )
        )
        desc = "Cocher cette option pour importer également les fichiers .csv présents dans le dossier"
        self.addParameter(
            QgsProcessingParameterBoolean(self.INCLUDE_CSV, desc, defaultValue=True)
        )
        desc = (
            "Rechercher dans les sous-dossiers. Utiliser cette option pour intégrer "
            "un dossier de N sous-dossiers de livrables. \nLe nom du sous-dossier sera "
            "utilisé comme import_prefix"
        )
        self.addParameter(
            QgsProcessingParameterBoolean(self.RECURSIVE, desc, defaultValue=False)
        )

        desc = (
            "\n\nMode d'import (creation d'une base ou ajout à une base existante)"
            '\nATTENTION, en mode APPEND, les fichiers "l_xxxx.csv" ne seront pas pris en compte '
            "(pour ne pas écraser les listes GRACE THD de destination"
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.APPEND,
                desc,
                [
                    "Append : Ajout à la base sqlite ciblée",
                    "Create : Creation d'une nouvelle base sqlite",
                ],
            )
        )

        desc = '\nBase de donnée .sqlite de destination (mode "Append")'
        desc += '\nNB: Paramètre inutilisé en mode "Create" : Dans ce mode, un fichier "base.sqlite" sera créé dans le dossier source'
        self.addParameter(
            # QgsProcessingParameterString(self.BASE_NAME,'Nom de la base de destination (sera ignoré en mode "append")', defaultValue='base.sqlite')
            QgsProcessingParameterFile(
                self.BASE_NAME,
                desc,
                behavior=QgsProcessingParameterFile.File,
                optional=True,
                extension="sqlite",
            )
        )
        desc = "Exporter les vues de contrôle (la liste des vues à exporter doit être paramétré dans la base)"
        self.addParameter(
            QgsProcessingParameterBoolean(self.EXPORT_VIEWS, desc, defaultValue=False)
        )
        desc = '\nPréfixe : cette option ajoute sur chaque couche une colonne "import_prefix" qui prendra pour valeur'
        desc += "\nla valeur indiquée en paramètre. Cette colonne permettra par la suite de gérer le versionning des livrables"
        desc += "\nPenser à utiliser des séparateurs par exemple : \n"
        desc += "AAAA-MM-JJ:NROXXX:SROYYY:VZ"
        self.addParameter(
            QgsProcessingParameterString(
                self.PREFIX, desc, defaultValue="", optional=True
            )
        )
        QgsProcessingParameterString

    def log(self, msg, log_level="info"):
        processing_logging.log(msg, self.logger, log_level)

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        # Récupération des inputs
        in_folder = self.parameterAsString(parameters, self.IN_FOLDER, context)
        append = self.parameterAsEnum(parameters, self.APPEND, context)

        base_name = self.parameterAsString(parameters, self.BASE_NAME, context)
        include_csv_ = self.parameterAsBool(parameters, self.INCLUDE_CSV, context)
        export_views = self.parameterAsBool(parameters, self.EXPORT_VIEWS, context)
        import_prefix = self.parameterAsString(parameters, self.PREFIX, context)
        recursive_ = self.parameterAsBool(parameters, self.RECURSIVE, context)

        log_folder = os.path.dirname(base_name) if base_name != "" else in_folder
        log_file = log_folder + "/log_integ.txt"

        # self.logger = logger.init_Logger(console = False, file = log_file, loggerName = 'processing', logLevel = logging.DEBUG)
        self.logger = logging.getLogger("")
        self.logger.handlers = []
        self.logger.setLevel(logging.DEBUG)
        processing_logging.add_file_handler(self.logger, log_file, logging.DEBUG)
        processing_logging.log_in_qgis_processing = True
        processing_logging.log_in_qgis_processing_method = feedback.pushInfo

        # Création de la base de donnée
        # En mode create on crée une base base.sqlite dans le dossier source
        # En mode append on crée une sauvegarde de la base de donnée
        if append == 0:
            ogr_target_ = base_name
            ignore_grace_list_ = True
        elif append == 1:
            ogr_target_ = None
            ignore_grace_list_ = False

        spatialiteio.import_folder_to_spl(
            in_folder,
            ogr_target=ogr_target_,
            prefix=import_prefix,
            include_csv=include_csv_,
            ignore_grace_list=ignore_grace_list_,
            recursive=recursive_,
        )

        if export_views:
            self.log("Export des vues de contrôle", log_level="info")
            spatialiteio.export_preset_tables(
                ogr_target_, "tactis_list_views_to_export"
            )

        self.logger.handlers = []
        processing_logging.log_in_qgis_processing = False
        return {self.OUT_BDD_NAME: ogr_target_}
