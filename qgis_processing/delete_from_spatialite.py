#! python3  # noqa: E265

################################################################################
# This file is part of quickinteg.

# quickinteg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quickinteg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quickinteg.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import sys
from pathlib import Path
from shutil import copyfile

# Ajout du dossier racine au path pour permettre leur import. En effet le contenu
# du fichier est exécuté à l'ouverture de QGIS lors du chargement du module
# processing.
root_module_path = Path(__file__).parents[1]  # = dossier racine de mcd_check
if root_module_path not in sys.path:
    sys.path.append(str(root_module_path))
################################################################################

# QGIS
from PyQt5.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterEnum,
    QgsProcessingParameterString,
    QgsProcessingParameterFile,
)

# package
from quickinteg import spatialiteio, processing_logging


# ############################################################################
# ########## Classes ###############
# ##################################


class Delete_From_Spatialite(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    BASE_NAME = "BASE_NAME"
    PREFIX = "PREFIX"
    OUTPUT = "OUTPUT"
    COLUMN = "COLUMN"
    TYPE = "TYPE"

    def tr(self, string_to_translate: str) -> str:
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string_to_translate)

    def createInstance(self):
        return Delete_From_Spatialite()

    def name(self) -> str:
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "delete_from_spatialite_v2.0"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("delete_from_spatialite_v2.0")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Tactis_Quickinteg")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "Tactis_Quickinteg"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr(
            "Suprimer Les lignes des tables qui contiennent"
            + "\n la valeur indiquée dans import_prefix"
        )

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        desc = 'Choisir le fichier "*.sqlite" à traiter'
        self.addParameter(
            # QgsProcessingParameterString(self.BASE_NAME,'Nom de la base de destination (sera ignoré en mode "append")', defaultValue='base.sqlite')
            QgsProcessingParameterFile(
                self.BASE_NAME,
                desc,
                behavior=QgsProcessingParameterFile.File,
                extension="sqlite",
            )
        )

        desc = '\nValeur "import_prefix".\n'
        desc += 'Toutes les lignes dont la colonne "import_prefix" contiendra la valeur renseignée seront suprimées\n'
        self.addParameter(
            QgsProcessingParameterString(self.PREFIX, desc, defaultValue="")
        )
        choices = [
            "IMPORT_PREFIX (pour les bases crées avec les outils Tactis)",
            "NUM_PROJET (pour les bases sur le modèle Axione)",
        ]

        QgsProcessingParameterString
        desc = "Choisis parmi les colonnes"
        self.addParameter(
            QgsProcessingParameterEnum(self.COLUMN, desc, choices, False, None, False)
        )

        ega_choices = ["EGALITE STRICTE", "CONTIENT LA VALEUR"]
        desc = "Choisis le type d'égalité "
        self.addParameter(
            QgsProcessingParameterEnum(self.TYPE, desc, ega_choices, False, None, False)
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        processing_logging.log_in_qgis_processing = True
        processing_logging.log_in_qgis_processing_method = feedback.pushInfo

        base_name = self.parameterAsString(parameters, self.BASE_NAME, context)
        import_prefix = self.parameterAsString(parameters, self.PREFIX, context)
        column_chosen = self.parameterAsString(parameters, self.COLUMN, context)
        condition_chosen = self.parameterAsString(parameters, self.TYPE, context)
        column_name = ""

        if column_chosen == "0":
            column_name = "import_prefix"
        else:
            column_name = "NUM_PROJET"

        if condition_chosen == "0":
            condition_ = "EGAL"
        elif condition_chosen == "1":
            condition_ = "CONTIENT"

        # copie de la base de donnée originale en backup.
        feedback.pushInfo(
            "Création d une sauvegarde de la BDD (" + base_name + "_backup)"
        )
        copyfile(base_name, base_name + "_backup")

        spatialiteio.delete_rows(
            base_name, import_prefix, column=column_name, condition=condition_
        )
        return {self.OUTPUT: "success"}
