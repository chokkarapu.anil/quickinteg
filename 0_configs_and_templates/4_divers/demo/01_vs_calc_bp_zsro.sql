DROP VIEW IF EXISTS v_calc_bp_zsro_status;
CREATE VIEW v_calc_bp_zsro_status AS

SELECT
    zs_code, zs_r3_code , bp_typelog ,
    CASE
        WHEN bp_lt_code NOT NULL THEN 'IMMEUBLE'
        WHEN bp_pt_code NOT NULL THEN 'EXTERIEUR'
    END AS localisation,
    SUM(bp_statut = 'AVP') AS 'AVP',
    SUM(bp_statut = 'PRO') AS 'PRO',
    SUM(bp_statut = 'REC') AS 'REC'

FROM
(
SELECT * FROM "vs_elem_bp_pt_nd"
AS vue LEFT JOIN "t_zsro"
ON ST_Within(vue.geom,t_zsro.geom)
where zs_code not null
)

GROUP by zs_code , bp_typelog
UNION
SELECT
    zs_code, zs_r3_code , bp_typelog ,
    CASE
        WHEN bp_lt_code NOT NULL THEN 'IMMEUBLE'
        WHEN bp_pt_code NOT NULL THEN 'EXTERIEUR'
    END AS localisation,
    SUM(bp_statut = 'AVP') AS 'AVP',
    SUM(bp_statut = 'PRO') AS 'PRO',
    SUM(bp_statut = 'REC') AS 'REC'

FROM
(
SELECT * FROM "vs_elem_bp_lt_st_nd"
AS vue LEFT JOIN "t_zsro"
ON ST_Within(vue.geom,t_zsro.geom)
where zs_code not null
)

GROUP by zs_code , bp_typelog;


DROP VIEW IF EXISTS v_calc_bp_zsro_avct;
CREATE VIEW v_calc_bp_zsro_avct AS

SELECT
    zs_code, zs_r3_code , bp_typelog ,
    CASE
        WHEN bp_lt_code NOT NULL THEN 'IMMEUBLE'
        WHEN bp_pt_code NOT NULL THEN 'EXTERIEUR'
    END AS localisation,
    SUM(bp_avct = 'C') AS 'bp_avct C',
    SUM(bp_avct = 'E') AS 'bp_avct E',
	SUM(bp_avct is null ) AS 'bp_avct null'

FROM
(
SELECT * FROM "vs_elem_bp_pt_nd"
AS vue LEFT JOIN "t_zsro"
ON ST_Within(vue.geom,t_zsro.geom)
where zs_code not null
)

GROUP by zs_code , bp_typelog
UNION
SELECT
    zs_code, zs_r3_code , bp_typelog ,
    CASE
        WHEN bp_lt_code NOT NULL THEN 'IMMEUBLE'
        WHEN bp_pt_code NOT NULL THEN 'EXTERIEUR'
    END AS localisation,
    SUM(bp_avct = 'C') AS 'bp_avct C',
    SUM(bp_avct = 'E') AS 'bp_avct E',
	SUM(bp_avct is null ) AS 'bp_avct null'

FROM
(
SELECT * FROM "vs_elem_bp_lt_st_nd"
AS vue LEFT JOIN "t_zsro"
ON ST_Within(vue.geom,t_zsro.geom)
where zs_code not null
)

GROUP by zs_code , bp_typelog;


