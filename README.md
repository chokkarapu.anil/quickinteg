Author:
Anil Chokkarapu-Forked on 06/09/2021

# QuickInteg

[![pipeline status](https://gitlab.com/vlebert/quickinteg/badges/master/pipeline.svg)](https://gitlab.com/vlebert/quickinteg/-/commits/master)

[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://vlebert.gitlab.io/quickinteg/)

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

## Documentation

La documentaiton complète de Quickinteg est disponible à cette adresse :

<https://vlebert.gitlab.io/quickinteg/>

## Présentation

Quickinteg est un ensemble de librairies et outils qui permettent :

* L'intégration d'un livrable SIG (shp/csv) dans une base de données
* Le développement de contrôles de qualité de donnée et de contrôles métier
* L'automatisation des contrôles et l'export de rapport d'erreur.

Quickinteg fonctionne aujourd'hui sur **Spatialite**. L'utilisation de Quickinteg repose
sur la notion de **template**. Un template est un socle de travail composé de :

* Une base de donnée Spatialite paramétrée selon le modèle de donnée cible (liste
des tables, type des champs, listes de valeurs)
* Plusieurs requêtes de contrôles prédéfinies
* Un projet QGIS avec une symbologie

Quickinteg est composé d'outils administrateurs permettant de créer des templates
et d'outils utilsateurs permettant d'intégrer un livrable SIG dans un template et
de générer automatiquement un rapport d'erreur.

![image](docs/static/img/global_schema.png)
