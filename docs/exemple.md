# Exemple d'utilisation

## Fichiers  de l'exemple

Ci-joint une archive contenant deux templates livrés en exemple :

* Un template GRACE THD avec des contrôles simples et sans contrôles complexes
* Un template (MCD Axione) avec des contrôles complexes (pas de contrôles simples)

En pratique il est possible de mélanger les deux principes sur un même template.

[template_exemple.zip](static/documents/template_exemple.zip)

----

## Arborescence

Après avoir extrait le zip, vous trouverez l'arborescence suivante (il est conseillé de la respecter pour organiser la création des templates) :

```tree
└───template_exemple
    ├───Tactis_check_SQL
    │   ├───1_table_def
    │   │   ├───gracethd_v2.0.1custom-------------Dossier MCD GRACE THD
    │   │   │       gracethd_10_lists.sql
    │   │   │       gracethd_20_insert.sql
    │   │   │       gracethd_30_tables.sql
    │   │   │       gracethd_40_spatialite.sql
    │   │   │       gracethd_50_index.sql
    │   │   │       gracethd_61_vues_elem.sql
    │   │   │       gracethd_90_labo.sql
    │   │   │       gracethd_91_patchs.sql
    │   │   │
    │   │   └───mcd-axione------------------------Dossier MCD Axione
    │   │           01_axione_tables.sql
    │   │           02_axione_geom.sql
    │   │           99_list_export.sql------------Création de la table des vues à exporter
    │   │
    │   ├───2_config_controle
    │   │   ├───1_catalogue_ctrl_xls
    │   │   │       grace_grille_sql_ctrl_v4.2.xlsx--------Catalogue de contrôle GRACE THD
    │   │   │
    │   │   ├───2_config_ctrl_xls
    │   │   │       config_ctrl_AO_ARIEGE_EXE.xlsx---------Configuration de contrôle pour le template
    │   │   │
    │   │   └───3_config_ctrl_sql--------------------------Dossier vide. Sera alimenté par les scripts
    │   ├───3_synth_ctrl
    │   │       ctrl_synt_generic.sql----------------------Vue de synthèses des contrôles simples
    │   │                                                  (comprend la création de la table des vues à exporter)
    │   └───4_divers
    │       └───84_Vues_APS--------------------------------Dossier comprenant des contrôles complexes
    │               01_base_prises_bal_check_rac.sql
    │               03_base_prises_bal_racc_long.sql
    │               04_bpe_cable_di.sql
    │               05_zapm_check.sql
    │               99_list_export.sql---------------------Édition de la table des vues à exporter
    │
    └───Templates
        ├───template_84PD2_APS
        │       mcd-axione-v2.qgz---------------------------------Mise en forme QGIS
        │       template.conf-------------------------------------Configuration du template
        │
        └───template_ARIEGE_AO
                gracethd-spatialite-tactis-qgis3.4_cleanv2.qgz----Mise en forme QGIS
                template.conf-------------------------------------Configuration du template

```

----

## Fichiers de configuration du template

### template_84PD2_APS

`template.conf`

```
[template]
table_def    = ../../Tactis_check_SQL/1_table_def/mcd-axione


sql          = ['../../Tactis_check_SQL/4_divers/84_Vues_APS']
export_table = 'tactis_list_views_to_export'
```

### template_ARIEGE_AO

_template.conf_

```
[template]
table_def    = ../../Tactis_check_SQL/1_table_def/gracethd_v2.0.1custom
mcd_check    = {'catalogue' : '../../Tactis_check_SQL/2_config_controle/1_catalogue_ctrl_xls/grace_grille_sql_ctrl_v4.2.xlsx',
                'config'    : '../../Tactis_check_SQL/2_config_controle/2_config_ctrl_xls/config_ctrl_AO_ARIEGE_EXE.xlsx'}

sql          = ['../../Tactis_check_SQL/3_synth_ctrl/ctrl_synt_generic.sql']
export_table = 'tactis_list_views_to_export'
```

----

## Création du template

Il suffit d'utiliser l'outil **[template_create_from_cfg](/scripts-qgis)** avec le fichier de configuration du template.

![image](static/img/exemple_create_from_cfg.png)

Le log affiche le détail de la compilation du template avec un status OK

![image](static/img/exemple_create_from_cfg_log.png)

----

## Résultat

Une base de donnée spatialite a été créé dans le dossier du fichier de configuration.

On retrouve bien les différents éléments.

![image](static/img/exemple_db.png)

![image](static/img/exemple_db_list_views.png)

----

## Utilisation du template

Le template constitue donc une base vierge (une structure de donnée sans données).

> **ASTUCE** : A chaque intégration il est recommandé de faire une copie du template vers le répertoire d'intégration et conserver ainsi un template propre pour les prochaines intégrations. Le template propre ne sera modifier qu'en cas de modification dans les requêtes de contrôle.

Il suffit alors d'intégrer les livrables avec le script **[import_folder_to_spatialite](/scripts-qgis)**, en cochant la case pour exporter les vues de contrôle.

A titre d'exemple, voici un livrable GRACE THD pour tester le template template_ARIEGE_AO :

[Annexe_1_JDD_TPJ_07032019.zip](static/documents/Annexe_1_JDD_TPJ_07032019.zip)

_Extrait des résultats du contrôle_

| ID_Test       | Description                                                                                                       | Classe           | Attribut   | Nombre | Pourcent    |
|---------------|-------------------------------------------------------------------------------------------------------------------|------------------|------------|--------|-------------|
| att_0003      | Valeur nulle pour un attribut obligatoire                                                                         | t_adresse        | ad_nomvoie | 21     | 1,969981238 |
| att_0029      | Valeur nulle pour un attribut obligatoire                                                                         | t_adresse        | ad_racc    | 1      | 0,09380863  |
| att_0030      | Valeur nulle pour un attribut obligatoire                                                                         | t_adresse        | ad_batcode | 1066   | 100         |
| att_0040      | Valeur nulle pour un attribut obligatoire                                                                         | t_adresse        | ad_idatcab | 1053   | 98,7804878  |
| att_0042      | Valeur nulle pour un attribut obligatoire                                                                         | t_adresse        | ad_typzone | 1065   | 99,90619137 |
| att_0056      | Valeur nulle pour un attribut obligatoire                                                                         | t_organisme      | or_type    | 8      | 66,66666667 |
| att_0062      | Valeur nulle pour un attribut obligatoire                                                                         | t_organisme      | or_nomvoie | 12     | 100         |
| att_0066      | Valeur nulle pour un attribut obligatoire                                                                         | t_organisme      | or_postal  | 12     | 100         |
| att_0524      | Valeur nulle pour un attribut obligatoire                                                                         | t_cable          | cb_capafo  | 13     | 0,852459016 |
| att_0525      | Valeur nulle pour un attribut obligatoire                                                                         | t_cable          | cb_fo_disp | 13     | 0,852459016 |
| att_0526      | Valeur nulle pour un attribut obligatoire                                                                         | t_cable          | cb_fo_util | 13     | 0,852459016 |
| att_0527      | Valeur nulle pour un attribut obligatoire                                                                         | t_cable          | cb_modulo  | 13     | 0,852459016 |
| att_0528      | Valeur nulle pour un attribut obligatoire                                                                         | t_cable          | cb_diam    | 13     | 0,852459016 |
| att_0533      | Valeur nulle pour un attribut obligatoire                                                                         | t_cable          | cb_creadat | 1      | 0,06557377  |
| attpatch_0001 | Valeur nulle pour un attribut obligatoire                                                                         | t_cable_patch201 | cb_code    | 1212   | 387,2204473 |
| geom_0005     | Geometrie invalide                                                                                                | t_zpbo           | geom       | 15     | 5,395683453 |
| geom_0017     | cb_bp1 ne correspond pas à un ebp situé sur cb_nd1                                                                | t_cable          | cb_bp1     | 8      | 0,524590164 |
| geom_0018     | cb_bp2 ne correspond pas à un ebp situé sur cb_nd2                                                                | t_cable          | cb_bp2     | 3      | 0,196721311 |
| geom_0019     | La ZAPBO est en superposition avec une autre zapbo (tolérance = 2.0m²)                                            | t_zpbo           | geom       | 83     | 29,85611511 |
| geom_0020     | La ZAPBO n est pas complètement incluse dans la ZSRO dont elle dépend (tolérance = 0.5m)                          | t_zpbo           | geom       | 13     | 4,676258993 |
| geom_0022     | Le PBO désigné par zp_bp_code (t_zpbo_patch201) n'est pas situé dans la ZAPBO                                     | t_zpbo           | zp_bp_code | 13     | 4,676258993 |
| geom_0025     | Le noeud désigné par zp_nd_code n'est pas situé dans la ZAPBO                                                     | t_zpbo           | zp_nd_code | 13     | 4,676258993 |
| metier_0008   | st_nblines n'est pas égal à la somme des adresses comprises dans la ZASRO (ad_nblhab, ad_nblpro) - grace THD 2.01 | t_sitetech       | st_nblines | 1      | 25          |
| metier_0010   | Le SRO comprend moins de 300.0 prises (st_nblines)                                                                | t_sitetech       | st_nblines | 1      | 25          |
| metier_0041   | Le t_ebp est associé à plusieurs ZAPBO (t_zpbo_patch201.zp_bp_code)                                               | t_ebp            | bp_code    | 9      | 0,564617315 |
