# Présentation

Quickinteg est un ensemble de librairies et outils qui permettent :

* L'intégration d'un livrable SIG (shp/csv) dans une base de données
* Le développement de contrôles de qualité de donnée et de contrôles métier
* L'automatisation des contrôles et l'export de rapport d'erreur.

Quickinteg fonctionne aujourd'hui sur **Spatialite**. L'utilisation de Quickinteg repose
sur la notion de **template**. Un template est un socle de travail composé de :

* Une base de donnée Spatialite paramétrée selon le modèle de donnée cible (liste
des tables, type des champs, listes de valeurs)
* Plusieurs requêtes de contrôles prédéfinies
* Un projet QGIS avec une symbologie

Quickinteg est composé d'outils administrateurs permettant de créer des templates
et d'outils utilsateurs permettant d'intégrer un livrable SIG dans un template et
de générer automatiquement un rapport d'erreur.

![image](static/img/global_schema.png)
