
**Plan de la page :**

# Installation 

Les scripts "processings" sont des scripts peuvent être exécutés depuis Qgis via le panneau Processing. Ces scripts offrent une interface utilisateur (boîtes de dialogue pour la sélection des inputs) grâce à l'API Qgis.

Télécharger le dossier la dernière version de Quickingteg depuis la page des releases :

<https://gitlab.com/vlebert/quickinteg/-/releases>

Dézipper ensuite Quickinteg dans un répertoire d'installation (par exemple c:\Quickinteg).

![image](static/img/dl_quickinteg.png)

Dans Qgis, modifier les options suivantes :

*Préférences > Options > Traitement > Scripts > Répertoire des scripts*

Ajouter le dossier **quickinteg/qgis_processing** dans la liste des repertoires (le dossier racine)

```tree
├───quickinteg_x.x.x
│   ├─── qgis_processing -------> Dossier racine à ajouter dans les options QGIS
│   ├─── quickinteg
│   ├─── ...
│   ├─── ...
│   └─── ...
```
