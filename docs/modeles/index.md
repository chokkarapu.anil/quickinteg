# Présentation des templates

Un template est une base de donnée spatialite paramétrée de manière spécifique à Quickinteg afin d'automatiser le contrôle de livrables.

Un template est spécifique à un projet, ainsi qu'au niveau de livrable (APS, APD, DOE...). Il dépend du modèle de données et des règles d'ingénierie spécifique à chaque projet.

## Modèle de donnée

Un template contient la structure des tables (attributs et types) sans données. Cela permet de garantir que le format du modèle de donnée sera respecté (ou bien l'intégration aboutira sur un échec).

## Requêtes de contrôle

### Contrôles simples

#### Rapport d'erreur

L'objectif des contrôle simple est de restitué un rapport d'erreur complet de la forme suivante :

_Rapport d'erreur cible_

| id_test   |Description                                    |Classe    | Attribut | xx_code        |DETAIL_ERREUR        |
| :-------- | :-------------------------------------------  | :------- | :------- | :------        | :-------------------|
| list_0008 |Valeur non présente dans la liste l_noeud_type | t_noeud  | nd_type  | ND890000000415 | Valeur nd_type : XX |
| ...       |...                                            | ...      | ...      | ...            | ...                 |

Pour aboutir à ce résultat quickinteg permet le catalogages des points de contrôle unitaire

#### Catalogue de points de contrôle

Il s'agit d'un tableau Excel listant les points de contrôle unitaire ainsi que leur code :

_Catalogue de contrôles_

| id_test  | description                               | classe    | attribut   | xx_code | param1 | param2 | param3 | requ_princ            | condition          | detail_erreur      |
|----------|-------------------------------------------|-----------|------------|---------|--------|--------|--------|------------------------|--------------------|--------------------|
| att_0001 | Valeur nulle pour un attribut obligatoire | t_adresse | ad_code    | ad_code |        |        |        | SQL Requête principale | Condition d'erreur | Détail additionnel |
| att_0002 | Valeur nulle pour un attribut obligatoire | t_adresse | ad_ban_id  | ad_code |        |        |        | SQL Requête principale | Condition d'erreur | Détail additionnel |
| att_0003 | Valeur nulle pour un attribut obligatoire | t_adresse | ad_nomvoie | ad_code |        |        |        | SQL Requête principale | Condition d'erreur | Détail additionnel |

Voici deux exemple de catalogue de contrôles :

* [GRACE THD](../static/documents/grace_grille_sql_ctrl_v4.2.xlsx)
* [Autre MCD](../static/documents/siea_sog_cir_ctrl_1.0.xlsx)

#### Configuration de contrôle

Pour créer le template, il faudra écrire la configuration de contrôle, c'est à dire la liste des contrôle unitaire à retenir dans le template, ainsi que leur paramétrage. Il s'agit d'un nouveau tableau excel de la forme suivante :

_Configuration de contrôle_

| id_test | config | param1 | param2 | param3 |
|---------|--------|--------|--------|--------|
|Identifiant du test dans le catalogue| O ou N pour retenir ou non le test dans le template | Valeur du paramètre 1 | Valeur du paramètre 2 | Valeur du paramètre 3 |

[Exemple de configuration de contrôle](../static/documents/config_ctrl_min.xlsx)

### Contrôles complexes

Les contrôles simples renvoient donc une ligne "standardisée" par erreur unitaire. Parfois ce mode de fonctionnement n'est pas suffisant et il peut être utile de faire des contrôle plus complexes et "multilignes" par exemple :

* Des listes (liste des raccordements longs, des PBO par type, etc)
* Des synthèses

Ces contrôles complexes sont écrits en SQL peuvent également être ajoutés au template.

_Exemple de contrôle complexe : répartition des BPE par type et câble entrant_

| type_bpe     | 12FO | 24FO | 48FO | 72FO | 96FO | 144FO | 288FO | 432FO | 576FO | 720FO |
|--------------|------|------|------|------|------|-------|-------|-------|-------|-------|
| BPE          | 3    | 5    | 0    | 9    | 0    | 2     | 6     | 1     | 0     | 3     |
| BPE Immeuble | 30   | 8    | 2    | 0    | 0    | 0     | 0     | 0     | 0     | 0     |
| PBO          | 19   | 13   | 26   | 4    | 0    | 0     | 0     | 0     | 0     | 0     |

Ce tableau peut être calculé à partir du code SQL suivant :

```sql
DROP VIEW IF EXISTS v_calc_ftth_bpe_cbdi;
CREATE VIEW v_calc_ftth_bpe_cbdi AS

SELECT
    ftth_bpe.*,
    SUM(ST_Intersects(ftth_bpe.geom, ftth_cable.geom)) AS nb_cables_di,
    MAX(ftth_cable.capacite * ST_Intersects(ftth_bpe.geom, ftth_cable.geom)) AS capa_entrant,
    SUM(ST_Intersects(ST_Buffer(ftth_bpe.geom, 0.05), ftth_cable.geom)) AS nb_cables_di_buff,
    MAX(ftth_cable.capacite * ST_Intersects(ST_Buffer(ftth_bpe.geom, 0.05), ftth_cable.geom)) AS capa_entrant_buff
FROM ftth_bpe, ftth_cable
GROUP BY ftth_bpe.objectid;


DROP VIEW IF EXISTS v_calc_ftth_bpe_cbdi_cbrac;
CREATE VIEW v_calc_ftth_bpe_cbdi_cbrac AS

SELECT
    v_calc_ftth_bpe_cbdi.*,
    SUM(ST_Intersects(v_calc_ftth_bpe_cbdi.geom, ftth_cable_2fo.geom)) AS nb_cb_racc
FROM
    v_calc_ftth_bpe_cbdi, ftth_cable_2fo
GROUP BY v_calc_ftth_bpe_cbdi.objectid;


DROP VIEW IF EXISTS v_calc_ftth_bpe_synth;
CREATE VIEW v_calc_ftth_bpe_synth AS

SELECT
    v_calc_ftth_bpe_cbdi_cbrac.*,
    CASE
        WHEN nb_cb_racc > 0 AND nb_cables_di_buff = 1 THEN 'PBO'
        WHEN nb_cb_racc > 0 AND nb_cables_di_buff > 1 THEN 'BPE + PBO'
        WHEN nb_cb_racc = 0  AND nb_cables_di_buff > 1 THEN 'BPE'
  WHEN nb_cb_racc = 0 AND nb_cables_di_buff = 1 AND type_pose != 'Immeuble' THEN 'Boite inutile'
        WHEN nb_cb_racc = 0 AND nb_cables_di_buff = 1 AND type_pose = 'Immeuble' THEN 'BPE Immeuble'
        ELSE 'NA'
    END AS type_bpe
FROM
    v_calc_ftth_bpe_cbdi_cbrac;

DROP VIEW IF EXISTS v_calc_ftth_bpe_synth2;
CREATE VIEW v_calc_ftth_bpe_synth2 AS

SELECT
    type_bpe,
    SUM(capa_entrant = 12) AS '12FO',
    SUM(capa_entrant = 24) AS '24FO',
    SUM(capa_entrant = 48) AS '48FO',
    SUM(capa_entrant = 72) AS '72FO',
    SUM(capa_entrant = 96) AS '96FO',
    SUM(capa_entrant = 144) AS '144FO',
    SUM(capa_entrant = 288) AS '288FO',
    SUM(capa_entrant = 432) AS '432FO',
    SUM(capa_entrant = 576) AS '576FO',
    SUM(capa_entrant = 720) AS '720FO'
FROM
    v_calc_ftth_bpe_synth
GROUP BY type_bpe;

DROP VIEW IF EXISTS v_calc_ftth_dim_pbo;
CREATE VIEW v_calc_ftth_dim_pbo AS
SELECT
    nb_cb_racc  AS nb_prises,
    COUNT(*) AS nb_pbo
FROM "v_calc_ftth_bpe_synth"
WHERE type_bpe LIKE '%PBO%'
GROUP BY nb_cb_racc;
```

----

## Restitution des contrôles

A l'issue de l'intégration d'un livrable dans un template, les contrôles sont réalisés et exportés si l'utilisateur à coché l'option correspondante dans l'outil **[import_folder_to_spatialite](../scripts-qgis)**

En pratique, chaque contrôle est stocké dans la base de donnée template sous la forme d'une vue SQL. Une table contient la liste des vues à exporter de la manière suivante :

_Table listant les contrôles à réaliser et à exporter_

| view_name | file_name| sheet_name | folder | file_type |
|-----------|----------|------------|--------|-----------|
|Nom de la vue à calculer| Fichier de destination | Onglet de destination (excel) | Dossier de destination | Format (xlsx, csv, shp et sqlite)|

La création de cette table est présenté sur les pages sur la création des templates.

----

## Fichier de configuration de templates

Un template est défini par son fichier de configuration, au format suivant :

_Fichier de configuration de template_

```ini
[template]
table_def    = ../../Tactis_check_SQL/1_table_def/gracethd_v2.0.2beta
mcd_check    = {'catalogue' : '../../Tactis_check_SQL/2_config_controle/1_catalogue_ctrl_xls/grace_grille_sql_ctrl_v4.2.xlsx',
                'config'    : '../../Tactis_check_SQL/2_config_controle/2_config_ctrl_xls/config_ctrl_31_APS_v2.xlsx'}

sql          = ['../../Tactis_check_SQL/3_synth_ctrl/ctrl_synt_generic_grace2.02b.sql', '../../Tactis_check_SQL/4_divers/31']
export_table = 'tactis_list_views_to_export'
```

* **table_def** : Chemin vers le dossier comprenant la définition du MCD
* **mcd_check** : Chemin vers le catalogue de contrôle et la configuration de contrôle
* **sql** : Chemin (dossiers ou fichier .sql)vers les requêtes additionnelles diverses (contrôles complexes, et autre)

!!! tip "Astuce"
    Penser à utiliser des **chemins relatifs** (`../..` pour remonter dans l'arborescence) afin que les fichiers de configuration soient compatibles entre plusieurs postes.

A partir de ce fichier de configuration, le template peut être compilé en quelques clics avec le script [template_create_from_cfg](../scripts-qgis#template_create_from_cfg).
