# Requêtes SQL addtionnelles

Les requêtes additionnelles peuvent être de plusieurs sortes :

- [vues de synthèse](#vues-de-synthese)
- [contrôles complexes](#controles-complexes)
- [list des vues à exporter](#liste-des-vues-a-exporter)

## Vues de synthèse

Les contrôles issus du catalogue génèrent un rapport d'erreur avec une ligne par erreur ce qui peut constituer plusieurs milliers de lignes lorsqu'il y a des erreurs redondantes.

Une bonne pratique est d'ajouter un onglet qui reprendra une synthèse par catégorie d'erreur :

```sql
DROP VIEW IF EXISTS "v_ctrl_synt";
CREATE VIEW "v_ctrl_synt" AS
SELECT ID_Test, Description, Classe, Attribut, count(ID_Test) AS 'Nombre_erreurs'  FROM "v_ctrl_all"  GROUP BY ID_Test;
```

## Contrôles complexes

Il est également possible de créer un dossier de requêtes SQL qui comprendront les contrôles complexe (voir présentation des templates).

## Liste des vues à exporter

Enfin il est nécessaire d'insérer dans la table des vues à exporter les différents exports souhaités (voir [restitution des contrôles](../../#restitution-des-controles)).

_Code SQL pour éditer la table des vues à exporter_

```sql
/*On supprimme les lignes par couple "nom_de_fichier"/"type_de_fichier"
 pour ne pas insérer des lignes en doublons si on exécute plusieurs fois les différents scripts  SQL*/
DELETE FROM tactis_list_views_to_export
WHERE
    dest_file || file_type
    IN ('tactis_mcd_checkxlsx');

/*On insère les paramètre d'export souhaités. On peut mettre plusieurs fois le même fichiers en xlsx et mettre des nom d'onglet différents (sheet_name)*/

INSERT INTO tactis_list_views_to_export
VALUES
    /* view_name          file_name                  sheet_name               folder               file_type */

    ('v_ctrl_all' ,       'tactis_mcd_check',        'erreurs',               '/tactis_mcd_check',   'xlsx'),
    ('v_ctrl_synt' ,      'tactis_mcd_check',        'synthese_erreurs',      '/tactis_mcd_check',   'xlsx')
    ;
```

!!! note
    Ce code peut ce situer à plusieurs endroits dans les fichiers de configuration du template (vue de synthèse, contrôles complexes...).
