# Environnement de développement

## 1. Autoriser l'utilisation des environnements virtuels

Si ça n'est pas déjà fait, il faut autoriser l'utilisation des scripts au niveau de l'utilisateur courant afin de pouvoir utiliser les environnements virtuels.

Commande à exécuter dans Powershell en mode administrateur (puis quitter le mode admin avant de continuer) :

```powershell
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
```

## 2. Créer l'environnement virtuel

```powershell
# lister les versions de Python installées
py --list

# créer un environnement virtuel, par exemple avec Python 3.7
py -3.7 -m venv .venv   # attention ne fonctionne pas avec Python installé depuis le Windows Store

# activer l'environnement virtuel
.\.venv\Scripts\activate
```

## 3. Installer les dépendances et les outils

```powershell
# mettre à jour pip
python -m pip install -U pip

# installer les dépendances
python -m pip install -U -r requirements/base.txt
python -m pip install -U -r requirements/development.txt

# installer pre-commit pour garantir la cohérence et le respect des règles de code communes
pre-commit install
```

## 4. Installer les dépendances tierces

Compte-tenu de la gestion des dépendances tierces dans QGIS, suivre la procédure dédiée : [Installer les dépendances](../external_dependencies)

## Installation du CLI

Pour utiliser quickinteg en mode CLI, utiliser le script `cli_install.sh`

Deux commandes sont disponibles :

- `qi-create` pour créer un template depuis un fichier de configuration
- `qi-import` pour faire une intégration

Pour l'aide utiliser l'option `-h` :

```
>>> qi-import -h
usage: qi-import [-h] [-c] [-d DESTINATION] [-p PREFIX] [-e] [-r] source

positional arguments:
  source                Chemin vers le repertoir à intégrer

optional arguments:
  -h, --help            show this help message and exit
  -c, --no-csv          Exclure les fichiers CSV
  -d DESTINATION, --destination DESTINATION
                        Chemin vers une base de donnée existante (mode APPEND).
  -p PREFIX, --prefix PREFIX
                        Préfixe d'importation ajouté sur chaque table.
  -e, --export          Exporter les vues de contrôle
  -r, --recursive       Mode récursif (parcours les sous-dossiers).
```
