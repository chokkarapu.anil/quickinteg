# Publication d'une nouvelle version

On distingue deux types de versions :

- les versions **intermédiaires**, destinées à tester les évolutions en cours de développement
- les versions **stables** destinées aux utilisateurs finaux

## Versions intermédiaires

Les versions intermédiaires correspondent aux artifacts générés pour chaque commit publié sur la branche principale sur GitLab (`master`).

Pour les récupérer :

1. Se rendre dans le menu `CI/CD Pipelines` : <https://gitlab.com/vlebert/quickinteg/-/pipelines>
2. Télécharger l'artifact via le menu déroulant à droite, en regard de l'exécution correspondant au commit :

![gitlab_ci_artifact_quickinteg](../static/img/dl_quickinteg_from_pipelines.png "Télécharger la version de développement de QuickInteg")

## Versions stables

Pour publier une nouvelle version stable :

1. Mettre à jour le numéro de version dans le fichier `quickinteg/__about__.py`
2. Etiqueter (*tag*) le commit correspondant :
    - soit en ligne de commande. Exemple : `git tag -a 2.5.0 XXXXXXX -m "Version 2.5"` - cf. [doc](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
    - soit via l'interface graphique de GitHub Desktop (clic droit sur le commit dans l'onglet `history`) - cf. [doc](https://docs.github.com/en/free-pro-team@latest/desktop/contributing-and-collaborating-using-github-desktop/managing-tags)
3. Une nouvelle release est automatiquement publiée sur GitLab
4. Ajouter le zip de la version
5. Complèter la description et éventuellement les autres champs

Les versions publiées (*releases*) se trouvent ici : <https://gitlab.com/vlebert/quickinteg/-/releases>.
