# Dependencies upgrade workflow

This plugin has external dependencies:

- [xlsxwriter](https://pypi.org/project/XlsxWriter/)

Because it's still hard to install Python 3rd party packages from an index (for example <https://pypi.org>) into QGIS, especially on Windows or Mac systems (or even on Linux if we want to do it properly in a virtual environment).

Those packages are delivered as embedded with the plugin into the `external` subfolder, but are not stored into the repository to avoid duplicating code.

## Development workflow

Manage versions in the `requirements/embedded.txt` file, then:

```powershell
python -m pip install -U -r requirements/embedded.txt --no-deps -t quickinteg/external
```

Note: even if a package depends on others packages, check before is these "sub"-dependencies are not already included with QGIS.

## Related links

- <https://gis.stackexchange.com/questions/141320/installing-3rd-party-python-libraries-for-qgis-on-windows>
- <https://github.com/QGIS-Contribution/QGIS-ResourceSharing/issues/112>
