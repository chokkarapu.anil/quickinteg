# Documentation

Project uses MkDocs to generate documentation Markdown files, stored into the `docs` folder in the repository.

!!! info
    The documentation is automatically built and deployed through Gitlab CI to Gitlab Pages.

## Build documentation website

To build it:

```powershell
# install aditionnal dependencies
python -m pip install -U -r requirements/documentation.txt
# build it
mkdocs build
```

Open `build\mkdocs\site\index.html` in a web browser.
